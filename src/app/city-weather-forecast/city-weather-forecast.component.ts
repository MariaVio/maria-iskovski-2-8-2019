import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {HttpService} from '../service/http.service';
import {LocalStorageService} from '../service/local-storage.service';
import {City} from '../model/city.model';
import {DataStorageService} from '../service/data-storage.service';
import {DailyWeatherForcastModel} from '../model/daily-weather-forcast.model';

@Component({
  selector: 'app-city-weather-forecast',
  templateUrl: './city-weather-forecast.component.html',
  styleUrls: ['./city-weather-forecast.component.scss']
})
export class CityWeatherForecastComponent implements OnInit, OnChanges {

  readonly ADD_TO_FAVORITE = 'Add to favorites';
  readonly REMOVE_FROM_FAVORITES = 'Remove from favorites';
  BASE_ICON_URL = '../assets/icons/';


  @Input() city: City;
  cityWeatherForecastList: any[];
  favorite: boolean;
  button_toggle_favorite_txt: string;


  constructor(private http: HttpService, private localStorageService: LocalStorageService,
              private dataStorageService: DataStorageService) {
  }

  ngOnInit() {
    if (this.city) {
      this.updateComponentData(this.city.Key);
    }
  }

  private updateComponentData(key: string) {
    this.favorite = this.dataStorageService.findIndexOfElement(key) >= 0;
    this.http.getCityWeaterFor5Days(key).subscribe((result) => {
      this.cityWeatherForecastList = result['DailyForecasts'];
    });
    this.toggleButtonName();
  }

  private toggleButtonName() {
    if (this.favorite) {
      this.button_toggle_favorite_txt = this.REMOVE_FROM_FAVORITES;
    } else {
      this.button_toggle_favorite_txt = this.ADD_TO_FAVORITE;
    }
  }


  getIcon(): string {
    let res;
    if (this.city) {
      res = this.BASE_ICON_URL + this.cityWeatherForecastList[0].Day.Icon + '.png';
    }
    return res;
  }


  changeCityStatus(): void {
    if (this.favorite) {
      this.dataStorageService.removeFavoriteCityFromList(this.city.Key);
    }
    else {
      this.dataStorageService.addCityToFavorites(this.city);

    }
    this.favorite = !this.favorite;
    this.toggleButtonName();


  }

  ngOnChanges(changes: SimpleChanges): void {
    let city = <City>changes.city.currentValue;
    if (city) {
      let key = city.Key;
      this.updateComponentData(key);
    }

  }
}
