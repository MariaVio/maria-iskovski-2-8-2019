import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {WeatherComponent} from './weather.component';


const routes: Routes = [
  {path: ':index', component: WeatherComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule {
}
