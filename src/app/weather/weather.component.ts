import {Component, OnInit} from '@angular/core';
import {HttpService} from '../service/http.service';
import {City} from '../model/city.model';
import {ActivatedRoute} from '@angular/router';
import {DataStorageService} from '../service/data-storage.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  cityList: City[];
  input: string;
  city: City;
  index: number;
  /**
   * store in State obj
   * @type {string}
   */
  readonly DEFAULT_CITY_COUNTRY = 'Tel Aviv, Israel';

  constructor(private http: HttpService,
              private activatedRoute: ActivatedRoute,
              private dataStorage: DataStorageService) {
  }

  ngOnInit() {
    this.index = +this.activatedRoute.snapshot.params['index'] | -1;

    this.cityList = [];
    if (!this.fromFavoritesPage()) {
      this.input = this.DEFAULT_CITY_COUNTRY;
      this.getCity(this.DEFAULT_CITY_COUNTRY);
    } else {
      this.changeCityProperty();
    }

    this.activatedRoute.params.subscribe((params) => {
      this.index = +params['index'];
      this.changeCityProperty();
    });

  }


  private fromFavoritesPage() {
    return this.index >= 0;
  }

  private changeCityProperty() {
    let favoriteModel = this.dataStorage.favoritesCityList[this.index];
    if (favoriteModel) {
      let cityNameFromFavorites = favoriteModel.name;
      this.getCity(cityNameFromFavorites);
      this.input = cityNameFromFavorites;
    }
  }

  changeList(value) {
    if (value) {
      if (this.citySelected(value)) {
        this.getCity(value);
      } else {
        this.http.getAutocomplete(value).subscribe((res: City[]) => {
          this.cityList = res;
        });
      }
    }
  }

  private getCity(value) {
    let cities: City[];
    let namesList = value.split(', ');
    this.http.getAutocomplete(namesList[0]).subscribe(res => {
        this.cityList = res;
        if (namesList.length == 2) {
          cities = this.cityList.filter((city) => {
            return city.LocalizedName === namesList[0] && city.Country.LocalizedName === namesList[1];
          });
          if (cities && cities.length > 0) {
            this.city = cities[0];
          }
        }
      },
    );

  }

  private citySelected(value) {
    return value.indexOf(',') >= 0;

  }


}
