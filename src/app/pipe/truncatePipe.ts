import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'removeString'
})


export class RemoveString implements PipeTransform {
  transform(str: string, char: string): string {
    return str.substring(0, (str.indexOf(char)));
  }
}

