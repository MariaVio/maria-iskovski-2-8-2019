import {Component, Input, OnInit} from '@angular/core';
import {HttpService} from '../service/http.service';
import {DailyWeatherForcastModel} from '../model/daily-weather-forcast.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-favorite-city-forecast',
  templateUrl: './favorite-city-forecast.component.html',
  styleUrls: ['./favorite-city-forecast.component.scss']
})
export class FavoriteCityForecastComponent implements OnInit {
  @Input() name: string;
  @Input() key: string;
  @Input() index: number;
  BASE_ICON_URL = '../assets/icons/';

  dailyWeather: DailyWeatherForcastModel;

  constructor(private http: HttpService,
              private route: Router) {

  }

  ngOnInit() {
     this.http.getCityWeather(this.key).subscribe((result)=>{
       this.dailyWeather = result[0];
    });
  }

  navigate() {
    this.route.navigate(['/' + this.index]);
  }
  getIcon(): string {
    let res;
    if (this.dailyWeather) {
      res = this.BASE_ICON_URL + this.dailyWeather.WeatherIcon + '.png';
    }
    return res;
  }


}
