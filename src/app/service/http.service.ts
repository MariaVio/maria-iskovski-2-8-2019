import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {LocationModel} from '../model/location.model';
import {from, Observable, of, pipe} from 'rxjs';
import {DailyWeatherForcastModel} from '../model/daily-weather-forcast.model';
import {CacheService} from './cache.service';
import {map} from 'rxjs/internal/operators';

@Injectable()
export class HttpService {

  readonly BASE_URL_AUTOCOMPLETE = 'https://dataservice.accuweather.com/locations/v1/cities/autocomplete?';
  readonly BASE_URL_DAILY_WEATHER = 'https://dataservice.accuweather.com/currentconditions/v1/';
  readonly BASE_URL_WEATHER_FOR_5_DAYS = 'https://dataservice.accuweather.com/forecasts/v1/daily/5day/';
  // readonly API_KEY = '6xH1GG4rGkw2ge426HNGHJFk4OFTXpHK';
  // readonly API_KEY = 't6G7ZIzSFdDZ6ct7WuNhYeDW0xdgAh0t';
  readonly API_KEY = '0gznNKywulO52WZhS5BbaGmudAJ1bwLc';


  citiesObservable: Observable<LocationModel[]>;

  constructor(private http: HttpClient, private cacheService: CacheService) {
    this.citiesObservable = new Observable<LocationModel[]>();
  }


  getAutocomplete(input: string): Observable<LocationModel[]> {
    let resultFromCache = this.cacheService.getCitiesFromCache(input);
    if (resultFromCache) {
      return of(resultFromCache);
    }

    let url = this.BASE_URL_AUTOCOMPLETE + 'apikey=' + this.API_KEY + '&q=' + input + '&language=en';
    return this.http.get<LocationModel[]>(url).pipe(
      map((res) => {
        this.cacheService.addCitiesToCache(input, res);
        return res;
      })
    );


  }

  getCityWeaterFor5Days(key: string) {
    let resultFromCache = this.cacheService.getCityWeatherFor5DaysFromCache(key);
    if (resultFromCache) {
      return of(resultFromCache);
    }
    return this.http.get(this.BASE_URL_WEATHER_FOR_5_DAYS + key + '?apikey=' + this.API_KEY + '&language=en' + '&details=false&metric=true').pipe(
      map((result) => {
        this.cacheService.addCityWeatherFor5DaysToCache(key, result);
        return result;
      })
    );


  }

  getCityWeather(key: string) {
    let resultFromCache = this.cacheService.getCityWeatherFromCache(key);
    if (resultFromCache) {
      return of(resultFromCache);
    }
    return this.http.get<DailyWeatherForcastModel>(this.BASE_URL_DAILY_WEATHER + key + '?apikey=' + this.API_KEY + '&language=en&details=false')
      .pipe(
        map((result) => {
          this.cacheService.addCityWeatherToCache(key, result);
          return result;
        })
      );


  }
}



