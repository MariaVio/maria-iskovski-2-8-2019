import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {LocationModel} from '../model/location.model';
import {DailyWeatherForcastModel} from '../model/daily-weather-forcast.model';

@Injectable()

export class CacheService {
  cities: Map<string, LocationModel[]>;
  cityWeatherForecast: Map<string, DailyWeatherForcastModel>;
  cityWeatherForecastFor5Days: Map<string, any>;

  constructor() {
    this.cities = new Map();
    this.cityWeatherForecast = new Map();
    this.cityWeatherForecastFor5Days = new Map();
  }

  addCitiesToCache(key: string, value: LocationModel[]) {
    this.cities.set(key, value);
  }

  getCitiesFromCache(key: string): LocationModel[] {
    return this.cities.get(key);
  }

  addCityWeatherFor5DaysToCache(key: string, value: any) {
    this.cityWeatherForecastFor5Days.set(key, value);
  }

  getCityWeatherFor5DaysFromCache(key: string) {
    return this.cityWeatherForecastFor5Days.get(key);
  }

  addCityWeatherToCache(key: string, value: DailyWeatherForcastModel) {
    this.cityWeatherForecast.set(key, value);
  }

  getCityWeatherFromCache(key: string): DailyWeatherForcastModel {
    return this.cityWeatherForecast.get(key);
  }

}
