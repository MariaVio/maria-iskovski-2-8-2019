import {FavoriteModel} from '../model/favorite.model';

export class LocalStorageService {

  readonly FAVORITES_CITY = 'Favorites-city';

  addFavoriteCity(city: FavoriteModel) {
    let favorites = JSON.parse(localStorage.getItem(this.FAVORITES_CITY));
    if (!favorites) {
      favorites = [];
    }
    if (!favorites.find((el: FavoriteModel) => {
      return el.key == city.key;
    })) {
      favorites.push(city);
    }

    localStorage.setItem(this.FAVORITES_CITY, JSON.stringify(favorites));
  }

  getFavoriteCitieList(): FavoriteModel[] {
    let arr = JSON.parse(localStorage.getItem(this.FAVORITES_CITY));
    return arr;
  }

  removeFavoriteCityFromList(index: number) {
    let favorites = JSON.parse(localStorage.getItem(this.FAVORITES_CITY));
    favorites.splice(index, 1);
    localStorage.setItem(this.FAVORITES_CITY, JSON.stringify(favorites));

  }
}
