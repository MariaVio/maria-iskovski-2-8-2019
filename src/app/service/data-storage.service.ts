import {Injectable} from '@angular/core';
import {City} from '../model/city.model';
import {LocalStorageService} from './local-storage.service';
import {FavoriteModel} from '../model/favorite.model';

@Injectable()
export class DataStorageService {

  favoritesCityList: FavoriteModel[];

  constructor(private localStorageService: LocalStorageService) {
    this.favoritesCityList = this.localStorageService.getFavoriteCitieList();

  }


  addCityToFavorites(city: City): void {
    let favorite = new FavoriteModel(`${city.LocalizedName}, ${city.Country.LocalizedName}`, city.Key);
    if (!this.favoritesCityList) {
      this.favoritesCityList = [];

    }
    this.favoritesCityList.push(favorite);
    this.localStorageService.addFavoriteCity(favorite);
  }

  removeFavoriteCityFromList(key: string): void {
    let index = this.findIndexOfElement(key);
    this.favoritesCityList.splice(index, 1);
    this.localStorageService.removeFavoriteCityFromList(index);

  }

  findIndexOfElement(key: string): number {
    let index;
    if (this.favoritesCityList) {
      index = this.favoritesCityList.findIndex((el: FavoriteModel) => {
        return el.key === key;
      });
    }
    return index;
  }
}
