import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from './material.module';
import {WeatherComponent} from './weather/weather.component';
import {FavoritesComponent} from './favorites/favorites.component';
import {HttpService} from './service/http.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {CityWeatherForecastComponent} from './city-weather-forecast/city-weather-forecast.component';
import {DailyWeatherForecastComponent} from './daily-weather-forecast/daily-weather-forecast.component';
import {LocalStorageService} from './service/local-storage.service';
import {DataStorageService} from './service/data-storage.service';
import {WeatherRoutingModule} from './weather/weather-routing.module';
import {FavoriteCityForecastComponent} from './favorite-city-forecast/favorite-city-forecast.component';
import {RemoveString} from './pipe/truncatePipe';
import {CacheService} from './service/cache.service';

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    FavoritesComponent,
    DailyWeatherForecastComponent,
    CityWeatherForecastComponent,
    FavoriteCityForecastComponent,
    RemoveString
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    WeatherRoutingModule


  ],
  providers: [HttpService, LocalStorageService, DataStorageService,CacheService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
