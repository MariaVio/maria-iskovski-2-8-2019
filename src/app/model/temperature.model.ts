interface MetricModel {
  Value: number;
  Unit: string;
  UnitType: number;

}

export interface TemperatureModel {
  Imperial: MetricModel;
  Metric: MetricModel;
}
