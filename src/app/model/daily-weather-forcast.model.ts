import {TemperatureModel} from './temperature.model';

export interface DailyWeatherForcastModel {
  LocalObservationDateTime: string;
  EpochTime: string;
  WeatherIcon: number;
  WeatherText: string;
  HasPrecipitation: boolean;
  PrecipitationType: object;
  IsDayTime: boolean;
  Temperature: TemperatureModel;
}
