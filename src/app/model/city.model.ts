import {CountryModel} from './country.model';

export interface City {

  Key: string;
  LocalizedName: string;
  Country: CountryModel;

}
