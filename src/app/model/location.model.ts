import {CountryModel} from './country.model';

interface AdministrativeAreaModel {
  ID: string;
  LocalizedName: string;

}

export class LocationModel {
  Version: number;
  Key: string;
  Type: string;
  Rank: number;
  LocalizedName: string;
  Country: CountryModel;
  AdministrativeArea: AdministrativeAreaModel;


}
