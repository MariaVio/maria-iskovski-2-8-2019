import {Component, OnInit} from '@angular/core';
import {DataStorageService} from '../service/data-storage.service';
import {FavoriteModel} from '../model/favorite.model';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  favoritesList: FavoriteModel[];

  constructor(private dataStorageService: DataStorageService) {
    this.favoritesList = this.dataStorageService.favoritesCityList;
  }

  ngOnInit() {
  }

}
