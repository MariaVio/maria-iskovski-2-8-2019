import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-daily-weather-forecast',
  templateUrl: './daily-weather-forecast.html',
  styleUrls: ['./daily-weather-forecast.scss']
})
export class DailyWeatherForecastComponent implements OnInit {

  @Input() dailyWeather: any;
  BASE_ICON_URL = '../assets/icons/';

  constructor() {
  }

  ngOnInit() {
  }

  getIcon(): string {
    if (this.dailyWeather) {
      return this.BASE_ICON_URL + this.dailyWeather['Day']['Icon'] + '.png';
    }

  }

}
